package com.kabby.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kabby.app.domain.Memo;

public interface MemoRepository extends JpaRepository<Memo, String> {

}
