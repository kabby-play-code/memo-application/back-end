package com.kabby.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kabby.app.domain.Memo;
import com.kabby.app.repository.MemoRepository;


@Service
public class MemoService {

private final MemoRepository memoRepository;
	
	public MemoService(MemoRepository memoRepository) {
		this.memoRepository=memoRepository;
	}
	
	public void registerMemo(Memo memo) {
		memoRepository.save(memo);
	}
	
	public List<Memo> findAllMemo(){
		return memoRepository.findAll();
	}
	
	public void removeMemo(String id) {
		memoRepository.deleteById(id);
	}
	
	public void modifyMemo(Memo memo) {
		memoRepository.save(memo);
	}
	
	
}
