package com.kabby.app.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kabby.app.domain.Memo;
import com.kabby.app.service.MemoService;

@RestController
@RequestMapping("api/memo")
@CrossOrigin("*")
public class MemoController {

	
	private final MemoService memoService;
	
	public MemoController(MemoService memoService) {
		this.memoService=memoService;
		
	}
	
	/**
	 * 메모 추가 
	 * @param memo
	 */
	@PostMapping(value={"","/"})
	public void registerMemo(@RequestBody Memo memo) {
	
		memoService.registerMemo(memo);
		
	}
	
	/**
	 * 메모 리스트 조회
	 * @return List<Memo>
	 */
	@GetMapping(value={"","/"})
	public @ResponseBody List<Memo> retrieveMemo() {
	
		return memoService.findAllMemo();
		
	}
	
	
	
	/**
	 * 메모 리스트 삭제
	 * @return List<Memo>
	 */
	@DeleteMapping(value={"/{id}"})
	public void removeMemo(@PathVariable(name="id") String id) {
	
		memoService.removeMemo(id);
		
	}
	
	/**
	 * 메모 리스트 수정
	 * @param memo
	 */
	@PutMapping(value = {"", "/"})
	public void modifyMemo(@RequestBody Memo memo) {
		
		memoService.modifyMemo(memo);
		
	}
	
	
}
